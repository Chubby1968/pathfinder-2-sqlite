-- -*- mode:sql sql-product:sqlite -*-

INSERT INTO sizes (size_id, short_name) VALUES
  (1, 'Tiny'),
  (2, 'Small'),
  (3, 'Medium'),
  (4, 'Large'),
  (5, 'Huge'),
  (6, 'Gargantuan');
